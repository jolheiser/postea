package pulls

import (
	"io"
	"regexp"
)

var (
	pullGiteaURL = "https://github.com/go-gitea/gitea/pull/"
	pullRegex    = regexp.MustCompile(`#(\d+)\)`)
)

// Format formats input by replacing pull refs #1234 with markdown links [#1234](...pull/1234)
func Format(r io.Reader) ([]byte, error) {
	data, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}

	pullURL := pullGiteaURL

	repl := pullRegex.ReplaceAll(data, []byte(`[#$1](`+pullURL+`$1))`))
	return repl, nil
}
