package main

import (
	"bytes"
	"flag"
	"fmt"
	"os"
	"strings"

	"code.gitea.io/postea/pulls"
	"code.gitea.io/postea/release"

	"github.com/AlecAivazis/survey/v2"
)

func main() {
	outputFlag := flag.String("output", "content/post/release-of-${milestone}.md", "Output file")
	flag.Parse()

	latest, err := release.Latest()
	if err != nil {
		fmt.Printf("could not get latest release: %v", err)
	}

	questions := []*survey.Question{
		{
			Name: "milestone",
			Prompt: &survey.Input{
				Message: "Milestone",
				Default: strings.TrimPrefix(latest.Name, "v"),
				Help:    fmt.Sprintf("Default was published on %s", latest.PublishedAt.Format("01/02/2006")),
			},
			Validate: survey.Required,
		},
		{
			Name: "author",
			Prompt: &survey.Input{
				Message: "Author",
				Default: os.Getenv("USER"),
			},
			Validate: survey.Required,
		},
	}

	answers := struct {
		Milestone string `survey:"milestone"`
		Author    string `survey:"author"`
		Changelog string `survey:"changelog"`
	}{}
	if err := survey.Ask(questions, &answers); err != nil {
		fmt.Println(err)
		return
	}

	if err := release.OpenChangelogPullRequest(answers.Milestone); err != nil {
		fmt.Println(err)
	}

	question := &survey.Editor{
		Message:  "Changelog",
		FileName: "*.md",
	}
	if err := survey.AskOne(question, &answers.Changelog); err != nil {
		fmt.Println(err)
		return
	}

	post, err := release.Format(answers.Author, answers.Milestone, answers.Changelog)
	if err != nil {
		fmt.Println(err)
		return
	}

	complete, err := pulls.Format(bytes.NewReader(post))
	if err != nil {
		fmt.Println(err)
		return
	}

	output := os.Expand(*outputFlag, func(s string) string {
		switch s {
		case "milestone":
			return answers.Milestone
		default:
			return s
		}
	})

	fi, err := os.Create(output)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer fi.Close()

	if _, err := fi.Write(complete); err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Blog post created at %q\n", output)
}
